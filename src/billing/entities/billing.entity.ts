import { OrderItem } from 'src/orders/entities/order-item';
import { Order } from 'src/orders/entities/order.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Billing {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float' })
  total_price: number;

  @Column({ type: 'float' })
  cash: number;

  @Column({ type: 'float' })
  change: number;

  @CreateDateColumn()
  createdDate: Date;

  @OneToOne(() => Order, (order) => order.billing)
  @JoinColumn()
  order: Order;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.status)
  orderItem: OrderItem[];

  @Column()
  role: number;
}
