import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class CreateBillingDto {
  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  total_price: number;

  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  cash: number;

  @IsNotEmpty()
  @IsNumber()
  change: number;

  @IsNotEmpty()
  orderId: number;

  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  role: number;
}
