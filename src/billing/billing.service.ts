import { Injectable } from '@nestjs/common';
import { CreateBillingDto } from './dto/create-billing.dto';
import { UpdateBillingDto } from './dto/update-billing.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Billing } from './entities/billing.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BillingService {
  constructor(
    @InjectRepository(Billing)
    private billingRespository: Repository<Billing>,
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
  ) {}

  async create(createBillingDto: CreateBillingDto) {
    const billing = new Billing();
    billing.total_price = createBillingDto.total_price;
    billing.cash = createBillingDto.cash;
    billing.change = createBillingDto.change;
    // billing.order = await this.ordersRepository.findOneBy({
    //   id: createBillingDto.orderId,
    // });
    billing.role = createBillingDto.role;
    return this.billingRespository.save(billing);
  }

  findAll() {
    return this.billingRespository.find({});
  }

  findOne(id: number) {
    return this.billingRespository.findOneBy({ id });
  }

  async update(id: number, updateBillingDto: UpdateBillingDto) {
    await this.billingRespository.update(id, updateBillingDto);
    return this.billingRespository.findOneBy({ id });
  }

  remove(id: number) {
    return this.billingRespository.softRemove({ id });
  }
}
