import { IsNotEmpty } from 'class-validator';
export class CreateTableDto {
  @IsNotEmpty()
  status: string;
}
