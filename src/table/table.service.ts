import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TableService {
  constructor(
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}

  create(createTableDto: CreateTableDto) {
    return this.tablesRepository.save(createTableDto);
  }

  findAll() {
    return this.tablesRepository.find();
  }

  // findOne(id: number) {
  //   return this.tablesRepository.findOne({ where: { id: id } });
  // }
  async findOne(id: number) {
    const table = await this.tablesRepository.findOne({
      where: { id: id },
      relations: ['order'],
    });
    if (!table) {
      throw new NotFoundException();
    }
    return table;
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    //Find id
    const table = await this.tablesRepository.findOneBy({ id: id });

    //CheckError
    if (!table) {
      throw new NotFoundException();
    }

    //update
    const updadetedTable = { ...table, ...updateTableDto };
    return this.tablesRepository.save(updadetedTable);
  }

  async remove(id: number) {
    //Find id
    const table = await this.tablesRepository.findOneBy({ id: id });

    //CheckError
    if (!table) {
      throw new NotFoundException();
    }

    //delete
    return this.tablesRepository.softRemove(table);
  }
}
