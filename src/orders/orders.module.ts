import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/order-item';
import { Product } from 'src/products/entities/product.entity';
import { Table } from 'src/table/entities/table.entity';
import { Status } from 'src/status/entities/status.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Order, OrderItem, Product, Table, Status]),
  ],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
