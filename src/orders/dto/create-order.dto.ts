import { IsNotEmpty, IsNumber } from 'class-validator';
import { Order } from '../entities/order.entity';

export class CreateOrderDto {
  tableId: number;
  orderItems: CreateOrderItemDto[];
}
class CreateOrderItemDto {
  // @IsString()
  // name: string;

  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  @IsNumber()
  qty: number;

  @IsNotEmpty()
  statusId: number;

  @IsNotEmpty()
  order: Order;
}
