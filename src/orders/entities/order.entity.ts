import { Table } from 'src/table/entities/table.entity';
import { OrderItem } from './order-item';
import { Billing } from 'src/billing/entities/billing.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

@Entity()
export class Order {
  [x: string]: any;
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amount: number;

  @Column()
  tableId: number;

  @Column({ type: 'float' })
  total: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  @ManyToOne(() => Table, (table) => table.order)
  table: Table;

  @OneToOne(() => Billing, (billing) => billing.order)
  billing: Billing;

  // @ManyToOne(() => Employee, (employee) => employee.orders)
  // employee: Employee;
}
