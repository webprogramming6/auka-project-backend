import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from 'src/products/entities/product.entity';
import { Status } from 'src/status/entities/status.entity';
import { Table } from 'src/table/entities/table.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
    @InjectRepository(Status)
    private statusRepository: Repository<Status>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const table = await this.tablesRepository.findOneBy({
      id: createOrderDto.tableId,
    });

    // const table = createOrderDto.tableId;
    const order: Order = new Order();
    order.table = table;
    order.amount = 0;
    order.total = 0;
    await this.ordersRepository.save(order); // ได้ id

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.qty = od.qty;
      //
      orderItem.product = await this.productsRepository.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      //
      orderItem.status = await this.statusRepository.findOneBy({
        id: od.productId,
      });
      orderItem.statusId = orderItem.status.id;

      orderItem.total = orderItem.price * orderItem.qty;
      orderItem.order = order; // อ้างกลับ
      await this.orderItemsRepository.save(orderItem);
      order.amount = order.amount + orderItem.qty;
      order.total = order.total + orderItem.total;
    }
    await this.ordersRepository.save(order); // ได้ id
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['orderItems', 'table', 'billing'],
    });
  }

  async getOrderbyTableId(tableId: number) {
    const order = await this.ordersRepository.find({
      where: { tableId: tableId },
      relations: ['orderItems'],
    });
    if (!order) {
      throw new NotFoundException(
        `Order with table number ${tableId} not found`,
      );
    }
    return order;
  }

  async getAllOrderItem(id: number) {
    const orderItem = await this.orderItemsRepository.find({
      where: { id: id },
      // relations: ['orderItems', 'table'],
    });
    console.log('asdad');
    return orderItem;
  }

  // findOne(id: number) {
  //   return this.ordersRepository.findOne({
  //     where: { id: id },
  //     relations: ['orderItems'],
  //   });
  // }

  findByStatus(id: number) {
    return this.orderItemsRepository.find({ where: { statusId: id } });
  }
  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems', 'table', 'billing'],
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
      // relations: ['orderItems', 'table'],
    });
    if (updateOrderDto.tableId) {
      const table = await this.tablesRepository.findOneBy({
        id: updateOrderDto.tableId,
      });
      order.table = table;
    }
    if (updateOrderDto.tableId) {
      const table = await this.tablesRepository.findOneBy({
        id: updateOrderDto.tableId,
      });
      order.table = table;
      order.tableId = order.table.id;
      await this.ordersRepository.save(order);
    }
    if (!order) {
      throw new NotFoundException();
    }
    if (updateOrderDto.tableId) {
      const table = await this.tablesRepository.findOneBy({
        id: updateOrderDto.tableId,
      });
      order.table = table;
    }
    if (!!updateOrderDto.orderItems) {
      for (const od of updateOrderDto.orderItems) {
        console.log('order item : ', od);

        const orderItem = new OrderItem();
        orderItem.qty = od.qty;
        //
        orderItem.product = await this.productsRepository.findOneBy({
          id: od.productId,
        });
        orderItem.name = orderItem.product.name;
        orderItem.price = orderItem.product.price;
        //
        orderItem.status = await this.statusRepository.findOneBy({
          id: od.statusId,
        });
        orderItem.statusId = orderItem.status.id;

        orderItem.total = orderItem.price * orderItem.qty;
        orderItem.order = order;
        // order.orderItems.push(orderItem);
        await this.orderItemsRepository.save(orderItem);
        order.amount = order.amount + orderItem.qty;
        order.total = order.total + orderItem.total;
        await this.ordersRepository.save(order); // ได้ id
      }
    }
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems'],
    });
  }

  async getOrderItemById(id: number) {
    const orderItem = await this.orderItemsRepository.findOne({
      where: { id: id },
    });
    return orderItem;
  }
  async getOrderItemByOrderId(id: number) {
    const orderItem = await this.orderItemsRepository.findOne({
      // where: { order.orderId : id },
    });
    return orderItem;
  }
  // async updateOrderItem(id: number, updateOrderDto: UpdateOrderDto) {
  //   const orderItem = await this.orderItemsRepository.findOne({
  //     where: { id: id },
  //   });
  //   console.log(updateOrderDto);
  //   for (const od of updateOrderDto.orderItems) {
  //     console.log('JSON : ', od);
  //     // const orderItem = new OrderItem();
  //     orderItem.id = id;
  //     orderItem.qty = od.qty;
  //     //
  //     orderItem.product = await this.productsRepository.findOneBy({
  //       id: od.productId,
  //     });
  //     orderItem.name = orderItem.product.name;
  //     orderItem.price = orderItem.product.price;
  //     //
  //     orderItem.status = await this.statusRepository.findOneBy({
  //       id: od.statusId,
  //     });
  //     orderItem.statusId = orderItem.status.id;

  //     orderItem.total = orderItem.price * orderItem.qty;
  //     // console.log('orderItem : ', orderItem);
  //     await this.orderItemsRepository.save(orderItem);
  //   }
  //   // orderItem.id =
  //   return this.orderItemsRepository.findOne({
  //     where: { id: id },
  //   });
  // }

  async updateOrderItem(
    id: number,
    updateOrderDto: {
      id: number;
      productId: number;
      qty: number;
      statusId: number;
    },
  ) {
    const orderItem = await this.orderItemsRepository.findOne({
      where: { id: id },
    });
    orderItem.statusId = updateOrderDto.statusId;
    await this.orderItemsRepository.save(orderItem);
    return this.orderItemsRepository.findOne({
      where: { id: id },
    });
  }

  // async chengeTable(
  //   id: number,
  //   updateOrderDto: {
  //     id: number;
  //     tableId: number;
  //   },
  // ) {
  //   const orderItem = await this.orderItemsRepository.findOne({
  //     where: { id: id },
  //   });
  //   orderItem.statusId = updateOrderDto.tableId;
  //   await this.orderItemsRepository.save(orderItem);
  //   return this.orderItemsRepository.findOne({
  //     where: { id: id },
  //   });
  // }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.ordersRepository.softRemove(order);
  }

  async removeOrderItem(id: number) {
    const orderItem = await this.orderItemsRepository.findOneBy({ id: id });
    return this.orderItemsRepository.softRemove(orderItem);
  }

  // async remove(id: number) {
  //   const order = await this.ordersRepository.findOneBy({ id: id });
  //   return this.ordersRepository.softRemove(order);
  // }
}
