import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  create(@Body() createOrderDto: CreateOrderDto) {
    return this.ordersService.create(createOrderDto);
  }

  @Get('status/:id')
  findByStatus(@Param('id') id: string) {
    return this.ordersService.findByStatus(+id);
  }

  @Get('orderItem/:id')
  getOrderItemById(@Param('id') id: string) {
    return this.ordersService.getOrderItemById(+id);
  }

  @Get('orderItems/order/:id')
  getOrderItemByOrderId(@Param('id') id: string) {
    return this.ordersService.getOrderItemByOrderId(+id);
  }

  @Get('table/:id')
  getOrderbyTableId(@Param('id') id: string) {
    return this.ordersService.getOrderbyTableId(+id);
  }

  @Get()
  findAll() {
    return this.ordersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ordersService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.ordersService.update(+id, updateOrderDto);
  }

  @Patch('/orderItem/:id')
  updateOrderItem(
    @Param('id') id: string,
    @Body()
    updateOrderItemDto: {
      id: number;
      productId: number;
      qty: number;
      statusId: number;
    },
  ) {
    return this.ordersService.updateOrderItem(+id, updateOrderItemDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ordersService.remove(+id);
  }

  @Delete('/orderItem/:id')
  removeOrderItem(@Param('id') id: string) {
    return this.ordersService.removeOrderItem(+id);
  }
}
