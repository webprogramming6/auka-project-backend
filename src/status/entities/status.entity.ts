import { OrderItem } from 'src/orders/entities/order-item';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Status {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.status)
  orderItem: OrderItem[];
}
