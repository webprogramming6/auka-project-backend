import { IsNotEmpty } from 'class-validator';

export class CreateStatusDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  name: string;
}
