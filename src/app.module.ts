import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { UserModule } from './user/user.module';
import { User } from './user/entities/user.entity';
import { TableModule } from './table/table.module';
import { Table } from './table/entities/table.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { Category } from './categories/entities/category.entity';
import { CategoriesModule } from './categories/categories.module';
import { Order } from './orders/entities/order.entity';
import { OrdersModule } from './orders/orders.module';
import { OrderItem } from './orders/entities/order-item';
import { Status } from './status/entities/status.entity';
import { AuthModule } from './auth/auth.module';
import { StatusModule } from './status/status.module';
import { BillingModule } from './billing/billing.module';
import { Billing } from './billing/entities/billing.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',

      database: 'db.sqlite',
      entities: [
        User,
        Table,
        Product,
        Category,
        Order,
        OrderItem,
        Status,
        Billing,
      ],
      synchronize: true,
    }),
    UserModule,
    TableModule,
    ProductsModule,
    CategoriesModule,
    OrdersModule,
    AuthModule,
    StatusModule,
    BillingModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
