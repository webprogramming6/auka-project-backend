import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
const saltOrRounds = 10;

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createUserDto.password, salt);
    createUserDto.password = hash;
    return this.usersRepository.save(createUserDto);
  }

  findAll() {
    return this.usersRepository.find();
  }

  findOne(id: number) {
    return this.usersRepository.findOne({ where: { id: id } });
  }

  findOneByEmail(login: string) {
    return this.usersRepository.findOne({ where: { login: login } });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    //Find
    const user = await this.usersRepository.findOneBy({ id: id });

    //CheckError
    if (!user) {
      throw new NotFoundException();
    }

    //update
    const updadetedUser = { ...user, ...updateUserDto };
    return this.usersRepository.save(updadetedUser);
  }

  async remove(id: number) {
    //Find
    const user = await this.usersRepository.findOneBy({ id: id });

    //CheckError
    if (!user) {
      throw new NotFoundException();
    }

    //delete
    return this.usersRepository.softRemove(user);
  }
}
